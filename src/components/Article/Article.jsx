import React from 'react'
import {Link} from 'react-router-dom'
import './Article.css'


export default function Article (props) {
  const {deleteHandler} = props

  return (
    <div className="article-item">
      <Link to={`item/${props.id}`} className="title">
        {props.title}
      </Link>
      <span className="comments-amount">
        {props.commentsAmount}
      </span>
      <span className="delete-item" onClick={deleteHandler} id={props.id}>
        Delete
      </span>
    </div>
  )
}