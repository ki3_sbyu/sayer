import React, {Component} from 'react'
import Article from '../Article/Article'
import './ArticleList.css'

class ArticleList extends Component {
  constructor(props) {
    super(props)
    this.state = {
      articles: this.props.articles
    }
  }

  deleteHandler = (e) => {
    const oldArticlesList = JSON.parse(localStorage.items)
    const newArticlesList = oldArticlesList.filter(
      x => +x.id !== +e.target.id
    )
    localStorage.items = JSON.stringify(newArticlesList)
    this.setState({
      articles: newArticlesList
    })
  }

  componentDidMount() {
    this.setState({
      articles: this.props.articles
    });
  }

  render() {
    const articles = this.state.articles
    let listItems
    if (articles) {
      listItems = articles.map( x => {
        return (
          <li key={x.id}>
            <Article
              title={x.title}
              commentsAmount={x.comments.length}
              id={x.id}
              deleteHandler={this.deleteHandler}
            />
          </li>
        )
      });
    }
    
    return (
      <div className="articles-list">
        <ul>
          {listItems}
        </ul>
      </div>
    )
  }
}

export default ArticleList