import React from 'react'
import './Comment.css'

export default function Comment(props) {
  return (
    <div className="comment">
      <div className="img"></div>
      <div className="text">
        {props.text}
      </div>
    </div>
  )
}