import React from 'react'
import './CommentsList.css'
import Comment from '../Comment/Comment'

class CommentsList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      comments: props.comments
    }
  }

  render() {
    const commentsList = this.state.comments.map(x => {
      return (
        <li key={x.id}>
          <Comment text={x.text}/>
        </li>)
    })
    return (
      <div className="comment-list">
        <ul>
          {commentsList}
        </ul>
      </div>
    )
  }
}

export default CommentsList