import React from 'react'
import ReactDOM from 'react-dom'
import Routing from './routing'

if (!localStorage.items) {
  localStorage.items = JSON.stringify([])
}

ReactDOM.render (
  <Routing />, document.getElementById('root')
)