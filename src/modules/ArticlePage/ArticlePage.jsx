import React from 'react'
import {Link} from 'react-router-dom'
import './ArticlePage.css'
import CommentsList from '../../components/CommentsList/CommentsList'

class ArticlePage extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      id: this.props.match.params.id,
      collection: JSON.parse(localStorage.items),
    }

    const item = this.state.collection.filter(x => x.id === +this.state.id).pop()
    if (item) {
      this.state.title = item.title
      this.state.comments = item.comments
    }
  }

  handleChange = e => {
    this.setState({
      value: e.target.value
    })
  }

  handleSend = e => {
    e.preventDefault()
    const collection = this.state.collection
    const item = collection.filter(x => x.id === +this.state.id).pop()
    const itemIndex = collection.indexOf(item)
    this.state.collection[itemIndex].comments.push({
      id: `cmnt${Date.now()}`,
      text: this.state.value
    })
    localStorage.items = JSON.stringify(collection);
    this.setState({
      collection: collection
    })
  }

  render() {
    if (this.state.comments) {
      return (
        <div className="article-page">
          <header>
            <Link to="/" className="back">⟵</Link>
            <h2>{this.state.title}</h2>
          </header>
          <div className="comments">
            <CommentsList comments={this.state.comments}/>
          </div>
          <form className="add-comment" onSubmit={this.handleSend}>
            <input type="text" placeholder="Type some text here" onChange={this.handleChange}/>
            <input type="submit" className="send" value=">"/>
          </form>
        </div>
      )
    } else {
      return (
        <h1>404</h1>
      )
    }
  }
}

export default ArticlePage