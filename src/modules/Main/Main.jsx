import React from 'react'
import ArticleList from '../../components/ArticleList/ArticleList'
import {Link} from 'react-router-dom'
import './Main.css'

function Main(props) {
  const articles = JSON.parse(localStorage.items)
  return (
    <div className="main-body">
      <header>
        <h1>
          Sayer
        </h1>
        <h4>
          World's most used time waster
        </h4>
      </header>
      <div className="content">
        <ArticleList articles={articles}/>
      </div>
      <div className="new-item">
        <Link to="/new">+</Link>
      </div>
    </div>
  )
}

export default Main