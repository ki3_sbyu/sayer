import React from 'react'
import {Link, withRouter} from 'react-router-dom'
import './NewArticle.css'

class NewArticle extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      value: ''
    }
  }

  handleChange = (event) => {
    this.setState({
      value: event.target.value
    });
  }

  handleSubmit = (event) => {
    event.preventDefault()
    if (!localStorage.items) {
      localStorage.items = JSON.stringify([])
    }
    const items = JSON.parse(localStorage.items)
    items.push({
      id: Date.now(),
      title: this.state.value.trim() === '' ? '' + Date.now(): this.state.value,
      comments: []
    })
    localStorage.items = JSON.stringify(items)

    this.props.history.push('/')
  }

  render() {
    return (
      <div className="new-article">
        <header>
          <Link className="back" to="/">⟵</Link>
          <h2>Create new item</h2>
        </header>
        <form className="content" onSubmit={this.handleSubmit}>
          <input type="text" placeholder="Type text here..." onChange={this.handleChange}/>
          <input type="submit" className="submit" value=">"/>
        </form>
      </div>
    )
  }
}

export default withRouter(NewArticle)