import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import ArticlePage from './modules/ArticlePage/ArticlePage'
import Main from './modules/Main/Main'
import NewArticle from './modules/NewArticle/NewArticle'

function NotFound(props) {
  return (
    <code>
      <h1>404</h1>
    </code>
  )
}

function Routing(props) {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Main}/>
        <Route path="/item/:id" component={ArticlePage}/>
        <Route path="/new" component={NewArticle}/>
        <Route component={NotFound}/>
      </Switch>
    </Router>
  )
}

export default Routing